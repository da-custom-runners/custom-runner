set -ev
SKIP=gitlabci-lint pre-commit run --all-files
pytest --doctest-modules
