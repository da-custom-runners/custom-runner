if [ -z "$SSH_PROXY" ]; then
    SSH_PROXY="ci-ssh.inria.fr"
fi

SSH_OPTIONS="-oProxyJump=$CI_USER_NAME@$SSH_PROXY -oStrictHostKeyChecking=no"

if [ -n "$TF_VAR_ORCHESTRATOR_VM_NAME" ]; then
  VM_NAME="$TF_VAR_ORCHESTRATOR_VM_NAME"
else
  VM_NAME="${CI_PROJECT_PATH/\//-}-orchestrator"
fi

mkdir -p /root/.ssh
cp infrastructure/known_hosts /root/.ssh/known_hosts
cp $SSH_PRIVATE_KEY /root/.ssh/id_rsa
chmod 0600 /root/.ssh/id_rsa
