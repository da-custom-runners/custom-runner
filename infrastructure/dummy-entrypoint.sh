#!/bin/sh

(
  echo "#### dummy-entrypoint ####"
  for arg in "$@" ; do
    cat <<EOF
$arg
####
EOF
  done
) >> /tmp/dummy-entrypoint.log

shift 2
exec "$@"
