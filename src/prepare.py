#!/builds/orchestrator/bin/python

# Standard library
import contextlib
import io
import json
import logging
import math
import os
import shlex
import socket
import time
import typing

# Third-party imports
import cs
import fabric
import invoke
import tenacity

# Project module
import base

N_RETRY = 10


def main(logger: logging.LoggerAdapter) -> None:
    job_info = base.get_job_info()
    if isinstance(job_info.image.contents, base.Test):
        test(logger, job_info, job_info.image.contents)
        return
    start_time = time.perf_counter()
    gitlab_response = base.get_gitlab_job_info(job_info.job_token)
    if gitlab_response.status_code == 200:
        queued_duration = gitlab_response.json()["queued_duration"]
    else:
        queued_duration = None
    gitlab_info = gitlab_response.json()
    logger.warning(
        f"Preparing image {job_info.image}...",
        extra={
            "image": job_info.image.contents.model_dump(),
            "queued_duration": queued_duration,
        },
    )
    config = base.Config.load()
    cloudstack = config.get_cloudstack()
    template = base.find_template(cloudstack, job_info.image.contents)
    vm = deploy_vm(
        logger,
        cloudstack,
        config,
        job_info,
        job_info.image.contents,
        template,
        base.VM_NAME_PREFIX,
    )
    vm_name = vm["name"]
    base.create_vm_tags(logger, cloudstack, vm, {base.RUNNER_TAG_KEY: "initialized"})
    connection = init_ssh_connection(logger, vm)
    cache_dir = base.get_cache_dir(job_info)
    init_cache(vm, cache_dir)
    connection = init_vm(logger, vm, connection, template, cache_dir)
    end_time = time.perf_counter()
    ellapsed_time = end_time - start_time
    logger.info(
        f"{vm_name} is ready after {ellapsed_time} seconds",
        extra={"vm_ready_ellapsed_time": ellapsed_time},
    )


def is_vm_free(vm) -> bool:
    return not base.has_tag(vm, base.JOB_ID_TAG_KEY)


def find_available_vms(
    cloudstack: cs.CloudStack, spec: base.Spec, template, prefix: str
):
    vms = [
        vm
        for vm in base.list_virtual_machines(
            cloudstack,
            prefix=prefix,
            templateid=template["id"],
            **base.build_tags({base.JOB_PREPARE_KEY: base.JOB_PREPARE_KEY}),
        )
        if base.get_vm_spec(cloudstack, vm) == spec and is_vm_free(vm)
    ]
    vms.sort(key=lambda vm: vm["created"])
    return vms


def take_vm(
    logger: logging.LoggerAdapter, cloudstack: cs.CloudStack, job_info: base.JobInfo, vm
) -> bool:
    if not base.try_create_vm_tags(
        logger, cloudstack, vm, {base.JOB_ID_TAG_KEY: job_info.job_id}
    ):
        return False
    base.create_vm_tags(
        logger,
        cloudstack,
        vm,
        {
            base.JOB_TOKEN_TAG_KEY: job_info.job_token,
        },
    )
    return True


def get_vms_to_destroy(cloudstack: cs.CloudStack, spec_needed: base.Spec, prefix: str):
    vms = [
        vm
        for vm in base.list_virtual_machines(cloudstack, prefix=prefix)
        if is_vm_free(vm)
    ]
    vms.sort(key=lambda vm: vm["created"])
    vms_prefix = []
    for vm in vms:
        if spec_needed.is_negative_or_null():
            break
        spec = base.get_vm_spec(cloudstack, vm)
        if spec is None:
            continue
        vms_prefix.append((vm, spec))
        spec_needed = spec_needed.sub(spec)
    vms_to_destroy = []
    for vm, spec in reversed(vms_prefix):
        spec_if_saved = spec_needed.add(spec)
        if spec_if_saved.is_negative_or_null():
            spec_needed = spec_if_saved
        else:
            vms_to_destroy.append(vm)
    return vms_to_destroy


def destroy_vms_if_possible_or_wait(
    logger: logging.LoggerAdapter,
    cloudstack: cs.CloudStack,
    spec: base.Spec,
    prefix: str,
) -> None:
    vms_to_destroy = [
        vm
        for vm in get_vms_to_destroy(cloudstack, spec, prefix)
        if base.try_create_vm_tags(
            logger, cloudstack, vm, {base.JOB_ID_TAG_KEY: "destroy"}
        )
    ]
    vms_in_error = base.list_virtual_machines(cloudstack, state="Error")
    vms_to_destroy.extend(vms_in_error)
    if vms_to_destroy == []:
        logger.warning(f"No unused virtual machine to destroy. Wait for 10s.")
        time.sleep(10)
        return
    base.destroy_vms(logger, cloudstack, vms_to_destroy)


def get_prepared_vm(
    logger: logging.LoggerAdapter,
    cloudstack: cs.CloudStack,
    job_info: base.JobInfo,
    vms,
):
    for vm in vms:
        if take_vm(logger, cloudstack, job_info, vm):
            base.delete_vm_tags(
                logger, cloudstack, vm, {base.JOB_PREPARE_KEY: base.JOB_PREPARE_KEY}
            )
            return vm
    return None


def deploy_vm(
    logger: logging.LoggerAdapter,
    cloudstack: cs.CloudStack,
    config: base.Config,
    job_info: base.JobInfo,
    image: base.CloudStack,
    template,
    prefix: str,
):
    spec = image.spec.adjust_for_template(logger, template, True)
    while True:
        vm = reuse_vm(logger, cloudstack, job_info, spec, template, prefix)
        if vm is not None:
            break
        vm_name = prefix + job_info.job_id
        vm = create_vm(
            logger, cloudstack, config, job_info, spec, template, vm_name, prefix
        )
        if vm is not None:
            break
    return vm


def reuse_vm(
    logger: logging.LoggerAdapter,
    cloudstack: cs.CloudStack,
    job_info: base.JobInfo,
    spec: base.Spec,
    template,
    prefix: str,
):
    vms = find_available_vms(cloudstack, spec, template, prefix)
    vm = get_prepared_vm(
        logger, cloudstack, job_info, [vm for vm in vms if vm["state"] == "Running"]
    )
    if vm is not None:
        notify_deployment(logger, cloudstack, vm["name"], " (prepared)")
        return vm
    vm = get_prepared_vm(
        logger, cloudstack, job_info, [vm for vm in vms if vm["state"] == "Starting"]
    )
    if vm is not None:
        vm_name = vm["name"]
        notify_deployment(logger, cloudstack, vm_name, " (in preparation)")
        try:
            vm = cloudstack.queryAsyncJobResult(jobId=vm["jobid"], fetch_result=True)[
                "virtualmachine"
            ]
        except cs.client.CloudStackApiException as e:
            logger.exception(f"Virtual machine {vm_name} not available anymore: {e}")
            return None
        return vm
    return None


def notify_deployment(
    logger: logging.LoggerAdapter, cloudstack: cs.CloudStack, vm_name: str, comment: str
) -> None:
    logger.warning(
        f"Deploying virtual machine {vm_name}{comment}...",
        extra=base.collect_cloudstack_metrics(cloudstack),
    )


def handle_deploy_failure(
    logger: logging.LoggerAdapter,
    cloudstack: cs.CloudStack,
    spec: base.Spec,
    prefix: str,
    exc: cs.client.CloudStackApiException,
) -> None:
    """
    This function handles failure cases for `create_vm`, when
    quotas have been exceeded: in this case, this function calls
    `destroy_vms_if_possible_or_wait` to try to make some room for the
    `VM`.
    Note that this behavior is specific for `create_vm`, when a job is
    actually waiting for the VM to be prepared. We don't need such a
    behavior when we create a VM by anticipation in `destroy.py`.
    """
    error_code = exc.error["errorcode"]
    extra = {"cloudstack_error_code": error_code}
    if error_code in (533, 535):
        # 533: no destination found for deployment (hypervisor exceeded)
        # 535: quota exceeded
        logger.exception(f"CloudStack capacity exceeded: {exc}.", extra=extra)
        destroy_vms_if_possible_or_wait(logger, cloudstack, spec, prefix)
    else:
        logger.exception(f"CloudStack failure: {exc}. Wait for 10s.", extra=extra)
        time.sleep(10)


def create_vm(
    logger: logging.LoggerAdapter,
    cloudstack: cs.CloudStack,
    config: base.Config,
    job_info: base.JobInfo,
    spec: base.Spec,
    template,
    vm_name: str,
    prefix: str,
) -> typing.Optional[base.VirtualMachine]:
    try:
        job = base.deploy_vm(logger, cloudstack, config, vm_name, spec, template)
    except cs.client.CloudStackApiException as e:
        handle_deploy_failure(logger, cloudstack, spec, prefix, e)
        return None
    vm = base.find_vm_by_id(cloudstack, job["id"])
    if not take_vm(logger, cloudstack, job_info, vm):
        return None
    start_time = time.perf_counter()
    notify_deployment(logger, cloudstack, vm_name, "")
    try:
        vm = base.wait_for_vm_deployment(logger, cloudstack, job)
    except cs.client.CloudStackApiException as e:
        logger.exception(f"CloudStack failure while creating the VM: {e}")
        base.destroy_vms(logger, cloudstack, [vm])
        handle_deploy_failure(logger, cloudstack, spec, prefix, e)
        return None
    end_time = time.perf_counter()
    ellapsed_time = end_time - start_time
    if isinstance(job_info.image.contents, base.CloudStack):
        extra = job_info.image.contents.model_dump()
    else:
        extra = spec.model_dump()
    extra["deploy_vm_ellapsed_time"] = ellapsed_time
    logger.warning(
        f"Deployed virtual machine {vm_name}.",
        extra=extra,
    )
    return vm


def init_ssh_connection(logger: logging.LoggerAdapter, vm) -> fabric.Connection:
    @tenacity.retry(
        retry=tenacity.retry_if_exception(base.is_connection_error),
        stop=tenacity.stop_after_attempt(N_RETRY),
        wait=tenacity.wait_exponential(max=60),
        before_sleep=base.custom_before_sleep_log(logger, logging.WARNING),
    )
    def get_connection(vm):
        connection = fabric.Connection(
            base.get_vm_ip_address(vm), user="ci", connect_kwargs={"password": "ci"}
        )
        connection.open()
        return connection

    return get_connection(vm)


def init_vm(
    logger: logging.LoggerAdapter,
    vm,
    connection: fabric.Connection,
    template,
    cache_dir: str,
) -> fabric.Connection:
    base.make_connection_in_stream_fast(connection)
    if template["hypervisor"] == "VMware":
        init_pf(logger, connection)
        connection = init_ssh_connection(logger, vm)
    else:
        init_docker_daemon(logger, connection)
    mount_cache(logger, connection, cache_dir)
    return connection


def init_pf(logger: logging.LoggerAdapter, connection: fabric.Connection) -> None:
    hostname = socket.gethostname()
    ip = socket.gethostbyname(hostname)
    rules = f"""
block all
pass in proto tcp from {ip} to port {{ 22 }}
pass out
"""
    connection.put(io.StringIO(rules), "ci")
    script = f"""
if echo ci | sudo -l -S  -p "" | grep -q ALL; then
    sudo chown root ci
    sudo chmod 644 ci
    sudo mv ci /etc/pf.anchors/
    echo ci | sudo -S pfctl -E -f /etc/pf.anchors/ci
else
    exit 1
fi
"""
    try:
        connection.run("bash", in_stream=io.StringIO(script), timeout=1)
    except invoke.exceptions.CommandTimedOut:
        pass
    except Exception as e:
        logger.exception(f"Cannot change firewall settings. VM connection is open. {e}")


def init_docker_daemon(
    logger: logging.LoggerAdapter, connection: fabric.Connection
) -> None:
    hostname = socket.gethostname()
    daemon_json = {
        "experimental": True,
        "registry-mirrors": [f"http://{hostname}:5000"],
    }
    daemon_json_str = shlex.quote(json.dumps(daemon_json))
    script = f"""
if [ ! -f /etc/docker/daemon.json ] && pgrep '^dockerd$' >/dev/null; then
    echo ci | sudo -l -S -p "" | grep -q ALL
    if [ ! -d /etc/docker ]; then
      sudo mkdir /etc/docker
    fi
    echo {daemon_json_str} >/tmp/daemon.json
    sudo mv /tmp/daemon.json /etc/docker/daemon.json
    sudo chown root /etc/docker/daemon.json
    sudo chmod 644 /etc/docker/daemon.json
    sudo pkill '^dockerd$'
fi
"""
    try:
        connection.run("bash", in_stream=io.StringIO(script))
    except Exception as e:
        logger.exception(f"Cannot set docker registry mirror")


def init_cache(vm, cache_dir: str) -> None:
    """
    Init the cache folder with the correct rights before mounting on VM
    """
    ip_address = base.get_vm_ip_address(vm)
    if not os.path.isdir(cache_dir):
        base.execute(["sudo", "mkdir", "-p", cache_dir])
    base.execute(["sudo", "chown", "-R", "ci:ci", cache_dir])
    base.execute(
        ["sudo", "exportfs", "-o", "rw,no_root_squash", f"{ip_address}:{cache_dir}"]
    )


def mount_cache(
    logger: logging.LoggerAdapter, connection: fabric.Connection, cache_dir: str
) -> None:
    hostname = socket.gethostname()
    mount_path = shlex.quote(f"{hostname}:{cache_dir}")
    script = f"""
echo ci | sudo -l -S -p "" | grep -q ALL
if [ ! -d /cache ]; then
    sudo mkdir /cache
fi
sudo mount {shlex.quote(mount_path)} /cache
"""
    try:
        connection.run("bash", in_stream=io.StringIO(script))
    except Exception as e:
        logger.exception(f"Cannot mount /cache by NFS: /cache is not shared. {e}")

    try:
        connection.run("bash", in_stream=io.StringIO(script))
    except Exception as e:
        logger.exception(f"Cannot fix permissions for /builds. {e}")


def get_unit_test_vm_prefix(job_info: base.JobInfo) -> str:
    return f"unit-test-{job_info.job_id}-"


def test_destroy_service_socket(
    logger: logging.LoggerAdapter, job_info: base.JobInfo, info: base.Test
) -> None:
    # Third-party imports
    import zmq

    # Project module
    import cleanup

    socket = cleanup.open_socket()
    socket.send_string("unit-test/destroy-service-socket")
    assert socket.recv_string() == "ok"


def test_create_virtual_machine(
    logger: logging.LoggerAdapter, job_info: base.JobInfo, info: base.Test
) -> None:
    unit_test_vm_prefix = get_unit_test_vm_prefix(job_info)
    config = base.Config.load()
    cloudstack = config.get_cloudstack()
    image = base.parse_image_name(
        "alpine,cpu:1,ram:2,disk:16", base.DEFAULT_SPEC, base.DEFAULT_OPTIONS
    )
    assert isinstance(image.contents, base.CloudStack)
    template = base.find_template(cloudstack, image.contents)
    spec = image.contents.spec.adjust_for_template(logger, template, True)
    vm_name = f"{unit_test_vm_prefix}create-virtual-machine"
    vm: typing.Optional[base.VirtualMachine] = None
    while vm is None:
        vm = create_vm(
            logger,
            cloudstack,
            config,
            job_info,
            spec,
            template,
            vm_name,
            unit_test_vm_prefix,
        )
    cloudstack.destroyVirtualMachine(id=vm["id"], expunge=True, fetch_result=True)


def test_handle_quotas(
    logger: logging.LoggerAdapter, job_info: base.JobInfo, info: base.Test
) -> None:
    unit_test_vm_prefix = get_unit_test_vm_prefix(job_info)
    config = base.Config.load()
    cloudstack = config.get_cloudstack()
    image = base.parse_image_name(
        "alpine,cpu:1,ram:2,disk:4", base.DEFAULT_SPEC, base.DEFAULT_OPTIONS
    )
    assert isinstance(image.contents, base.CloudStack)
    template = base.find_template(cloudstack, image.contents)
    vms = []
    try:
        counter = 0
        test_prefix = unit_test_vm_prefix + "handle-quotas-"
        cpu = 1
        ram = 2
        disk = 4
        last_spec: typing.Optional[base.Spec] = None
        while True:
            vm_name = test_prefix + str(counter)
            spec = base.Spec(cpu=cpu, ram=ram, disk=disk)
            vm = create_vm(
                logger,
                cloudstack,
                config,
                job_info,
                spec,
                template,
                vm_name,
                test_prefix,
            )
            if vm is None:
                if last_spec is None:
                    continue
                else:
                    break
            vms.append(vm)
            counter += 1
            last_spec = spec
            cpu = min(base.MAX_CPU, 2 * cpu)
            ram = min(base.MAX_RAM, 2 * ram)
            disk = min(base.MAX_DISK, 2 * disk)
        if vms != []:
            base.delete_vm_tags(
                logger, cloudstack, vms[-1], {base.JOB_ID_TAG_KEY: job_info.job_id}
            )
        vm = create_vm(
            logger,
            cloudstack,
            config,
            job_info,
            last_spec,
            template,
            vm_name,
            test_prefix,
        )
        if vm is None:
            vm = create_vm(
                logger,
                cloudstack,
                config,
                job_info,
                last_spec,
                template,
                vm_name,
                test_prefix,
            )
            assert vm is not None
        vms.append(vm)
    finally:
        base.destroy_vms(logger, cloudstack, vms)


def test_reuse(
    logger: logging.LoggerAdapter, job_info: base.JobInfo, info: base.Test
) -> None:
    unit_test_vm_prefix = get_unit_test_vm_prefix(job_info)
    config = base.Config.load()
    cloudstack = config.get_cloudstack()
    image = base.parse_image_name(
        "alpine,cpu:1,ram:2,disk:4", base.DEFAULT_SPEC, base.DEFAULT_OPTIONS
    )
    assert isinstance(image.contents, base.CloudStack)
    template = base.find_template(cloudstack, image.contents)
    spec = image.contents.spec.adjust_for_template(logger, template, True)
    vm_name = f"{unit_test_vm_prefix}test-reuse"
    vm0: typing.Optional[base.VirtualMachine] = None
    while vm0 is None:
        vm0 = create_vm(
            logger,
            cloudstack,
            config,
            job_info,
            spec,
            template,
            vm_name,
            unit_test_vm_prefix,
        )
    try:
        base.recycle_vm(logger, cloudstack, vm0, job_info)
        vm1 = reuse_vm(
            logger, cloudstack, job_info, spec, template, unit_test_vm_prefix
        )
        assert vm1 is not None
        assert vm0["id"] == vm1["id"]
    finally:
        base.destroy_vms(logger, cloudstack, [vm0])


TESTS = {
    "unit-test/destroy-service-socket": test_destroy_service_socket,
    "unit-test/create-virtual-machine": test_create_virtual_machine,
    "unit-test/handle-quotas": test_handle_quotas,
    "unit-test/reuse": test_reuse,
}


def test(
    logger: logging.LoggerAdapter, job_info: base.JobInfo, info: base.Test
) -> None:
    test_function = TESTS[info.name]
    test_function(logger, job_info, info)


if __name__ == "__main__":
    base.execute_with_loggers("prepare", main)
