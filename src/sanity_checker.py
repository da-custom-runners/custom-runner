#!/builds/orchestrator/bin/python

"""
This script is run by systemd timer (see
`infrastructure/deploy/sanity_checker.timer` and
`infrastructure/deploy/sanity_checker.service`).
"""

# Standard library
import datetime
import logging
from enum import Enum, auto

# Third-party imports
import dateutil.tz

# Project module
import base

EXPIRATION_TIME = datetime.timedelta(
    minutes=5
)  # Time before considerating that VM has expired
STOP_MAX_TIME = datetime.timedelta(
    minutes=5
)  # Time before considerating that stopped VM can be destroyed


class VMStatus(Enum):
    STOPPED = auto()
    ERROR = auto()
    RUNNING = auto()  # running correctly (not inconsistent, not expired)
    INCONSISTENT = auto()
    EXPIRED = auto()


def get_vm_last_updated_date(vm):
    last_updated = vm.get("lastupdated")
    if last_updated is None:
        return datetime.datetime.min
    else:
        return datetime.datetime.fromisoformat(last_updated)


def get_vm_status(logger: logging.LoggerAdapter, vm):
    vm_state = vm.get("state", "")
    now = datetime.datetime.now(dateutil.tz.tzlocal())

    if vm_state == "Stopped":
        # We only delete stopped VMs if they do not have been touch for a
        # short delay (5 minutes).  Sometimes, virtual machines appear
        # "Stopped" when they just have been created, before being
        # started.
        # For instance, it happened here: https://gitlab.inria.fr/coq/coq/-/jobs/3418664
        if now - get_vm_last_updated_date(vm) > STOP_MAX_TIME:
            return VMStatus.STOPPED

    elif vm_state == "Error":
        return VMStatus.ERROR

    elif vm_state == "Running":
        job_id = base.get_tag(vm, base.JOB_ID_TAG_KEY)
        job_token = base.get_tag(vm, base.JOB_TOKEN_TAG_KEY)
        vm_name = vm["name"]

        # Consider prepared vms
        if base.get_tag(vm, base.JOB_PREPARE_KEY) == base.JOB_PREPARE_KEY:
            if job_id or job_token:
                logger.error(
                    f"VM {vm_name} has both prepare and job tags", extra={"vm": vm_name}
                )
                return VMStatus.INCONSISTENT

            if now - get_vm_last_updated_date(vm) > EXPIRATION_TIME:
                return VMStatus.EXPIRED

            return VMStatus.RUNNING

        # Consider other vms (i.e., not prepared)
        if not job_token:
            logger.error(
                f"VM {vm_name} does not have a prepare or job tag",
                extra={"vm": vm_name},
            )
            return VMStatus.INCONSISTENT

        response = base.get_gitlab_job_info(job_token)
        if response.status_code != 200:
            logger.error(
                f"Failed to fetch job status for token {job_token} associated to {vm_name}. "
                f"HTTP status: {response.status_code}.",
                extra={"vm": vm_name},
            )
            return VMStatus.INCONSISTENT

        return VMStatus.RUNNING


def main(logger: logging.LoggerAdapter) -> None:
    cloudstack = base.Config.load().get_cloudstack()
    vms = base.list_virtual_machines(cloudstack, prefix=base.VM_NAME_PREFIX)
    running_vms = [vm for vm in vms if vm["state"] == "Running"]

    sorted_vms: dict[VMStatus, list] = {status: [] for status in VMStatus}

    for vm in vms:
        vm_status = get_vm_status(logger, vm)
        sorted_vms[vm_status].append(vm)

    extra = {
        "running_vms": len(running_vms),
        "expired_vms": len(sorted_vms[VMStatus.EXPIRED]),
        "inconsistent_vms": len(sorted_vms[VMStatus.INCONSISTENT]),
        "stopped_vms": len(sorted_vms[VMStatus.STOPPED]),
        "vms_in_error": len(sorted_vms[VMStatus.ERROR]),
    }
    logger.info(
        f"Checked {len(running_vms)} running virtual machines: "
        f"{extra['expired_vms']} expired,  "
        f"{extra['inconsistent_vms']} in inconsistent state, "
        f"{extra['stopped_vms']} stopped, "
        f"{extra['vms_in_error']} in error.",
        extra=extra,
    )
    vms_to_destroy = (
        sorted_vms[VMStatus.INCONSISTENT]
        + sorted_vms[VMStatus.STOPPED]
        + sorted_vms[VMStatus.ERROR]
        + sorted_vms[VMStatus.EXPIRED]
    )
    base.destroy_vms(logger, cloudstack, vms_to_destroy)


if __name__ == "__main__":
    base.execute_with_loggers("sanity_checker", main)
