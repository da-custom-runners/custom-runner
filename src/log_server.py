#!/builds/orchestrator/bin/python

"""
TCP socket-based server to receive log messages and write them to
a elasticsearch-compatible log file.

The code is essentially copied from

https://docs.python.org/3.11/howto/logging-cookbook.html#network-logging

according to the following advice

https://docs.python.org/3.11/howto/logging-cookbook.html#logging-to-a-single-file-from-multiple-processes

| Although logging is thread-safe, and logging to a single file from
| multiple threads in a single process is supported, logging to a single
| file from multiple processes is not supported, because there is no
| standard way to serialize access to a single file across multiple
| processes in Python. If you need to log to a single file from multiple
| processes, one way of doing this is to have all the processes log to a
| SocketHandler, and have a separate process which implements a socket
| server which reads from the socket and logs to file.

The target handler has been adapted to elasticsearch with the following
guide

https://www.elastic.co/guide/en/cloud/current/ec-getting-started-search-use-cases-python-logs.html
"""

# Standard library
import logging
import logging.handlers
import pickle
import socketserver
import struct
import typing

# Third-party imports
import ecs_logging


class LogRecordStreamHandler(socketserver.StreamRequestHandler):
    """Handler for a streaming logging request.

    This basically logs the record using whatever logging policy is
    configured locally.
    """

    def handle(self) -> None:
        """
        Handle multiple requests - each expected to be a 4-byte length,
        followed by the LogRecord in pickle format. Logs the record
        according to whatever policy is configured locally.
        """
        while True:
            chunk = self.connection.recv(4)
            if len(chunk) < 4:
                break
            slen = struct.unpack(">L", chunk)[0]
            chunk = self.connection.recv(slen)
            while len(chunk) < slen:
                chunk = chunk + self.connection.recv(slen - len(chunk))
            obj = self.unPickle(chunk)
            record = logging.makeLogRecord(obj)
            self.handleLogRecord(record)

    def unPickle(self, data) -> dict[str, typing.Any]:
        return pickle.loads(data)

    def handleLogRecord(self, record) -> None:
        logger = logging.getLogger(record.name)
        # N.B. EVERY record gets logged. This is because Logger.handle
        # is normally called AFTER logger-level filtering. If you want
        # to do filtering, do it at the client end to save wasting
        # cycles and network bandwidth!
        logger.handle(record)


class LogRecordSocketReceiver(socketserver.ThreadingTCPServer):
    """
    Simple TCP socket-based logging receiver suitable for testing.
    """

    allow_reuse_address = True

    def __init__(
        self,
        host="localhost",
        port=logging.handlers.DEFAULT_TCP_LOGGING_PORT,
        handler=LogRecordStreamHandler,
    ) -> None:
        super().__init__((host, port), handler)
        self.abort = 0
        self.timeout = 1

    def serve_until_stopped(self) -> None:
        import select

        abort = 0
        while not abort:
            rd, wr, ex = select.select([self.socket.fileno()], [], [], self.timeout)
            if rd:
                self.handle_request()
            abort = self.abort


def main() -> None:
    handler = logging.handlers.RotatingFileHandler(
        "/builds/elasticsearch/logs/custom-runner.log",
        maxBytes=1024**3,
        backupCount=1,
    )
    handler.setFormatter(ecs_logging.StdlibFormatter())
    logging.basicConfig(handlers=[handler], level=logging.DEBUG)
    tcpserver = LogRecordSocketReceiver()
    print("About to start TCP server...")
    tcpserver.serve_until_stopped()


if __name__ == "__main__":
    main()
