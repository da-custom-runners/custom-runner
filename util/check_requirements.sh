#!/bin/sh

set -e

pip-compile --quiet
git diff --exit-code requirements.txt
